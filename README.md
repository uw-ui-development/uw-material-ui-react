# uw-material-ui-react

UW Material UI React is a preconfigured UW-Madison theme for use with
[Material-UI](https://material-ui.com/)-based React projects.

![UW Material UI React](screenshots/uw-material-ui-react.png)

## Installation

In your project, create a `.npmrc` file at the project root, with the following contents:

```
@uw-ui-development:registry=https://git.doit.wisc.edu/api/v4/packages/npm/
```

Install with [Yarn](https://yarnpkg.com/lang/en/) or [npm](https://www.npmjs.com/):

```sh
$ yarn add @uw-ui-development/uw-material-ui-react
# or
$ npm install --save @uw-ui-development/uw-material-ui-react
```

## Example Usage

```jsx
import React from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import { theme } from "@uw-ui-development/uw-material-ui-react";

function App(props) {
  return (
    <ThemeProvider theme={theme}>
      <Button>{props.text}</Button>
    </ThemeProvider>
  );
}

ReactDOM.render(<App text="I'm blue!" />, document.getElementById('app'));
```