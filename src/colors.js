export const colorRed = "#c50505";
export const colorBlue = "#0479a8";
export const colorBlueHover = "#2a8db5";
export const colorBlackFont = "#282728";
export const colorGrayHover = "#e5e5e5";
export const colorGrayLight = "#adadad";
